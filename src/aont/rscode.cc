#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern "C" {
    #include <isa-l/erasure_code.h>
    #include <isa-l/types.h>
    #include <isa-l/test.h>
}

#include "rscode.hh"

// Reference: https://www.backblaze.com/blog/reed-solomon/

RSCode::RSCode(int n, int k){
	_n = n;
	_k = k;

	// Generate encode matrix
	gf_gen_rs_matrix(encode_matrix, _n, _k);

	// Generate galois_table from encode matrix
	ec_init_tables(_k, _n - _k, &encode_matrix[_k * _k], galois_table);
}

RSCode::~RSCode() {

}

int RSCode::getNumChunksPerNode() {
	return 1;
}

int RSCode::getChunkSize(unsigned long int dataSize) {
	return (dataSize + _k - 1) / _k;
}

int RSCode::getNumDataChunks() {
	return _k;
}

int RSCode::getNumCodeChunks() {
	return (_n - _k);
}

int RSCode::getMatrixSize() {
	return (_n * _k);
}

bool RSCode::encode(unsigned char *data, unsigned long int dataSize, unsigned char *code, unsigned long int codeSize, unsigned char *matrix) {
	// void ec_encode_data(int len, int k, int rows, unsigned char *gftbls,unsigned char **data, unsigned char **coding);

	return true;
}

bool RSCode::decode(unsigned char *input, unsigned long int inputSize, unsigned char *decodeData, unsigned long int &decodeSize, int *chunkIndices, int numChunks, unsigned char *encodingMatrix) {
	// get alive data row

	// inverse matrix of data

	// decode

	return true;
}