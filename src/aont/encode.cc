#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstring>

#include <openssl/aes.h>
#include <openssl/md5.h>
#include <openssl/crypto.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <openssl/evp.h>

#include "aont.hh"
#include "encode.hh"

using namespace std;

// ====================================================================================================

AONT_Encode::AONT_Encode(short key_size){
    AONT_BLOCK_SIZE = key_size;

    // Initiailze GF-Complete variables
    if (!gf_init_easy(&gf_object, 8)) {
        fprintf(stderr, "Error: bad gf specification\n");
        exit(1);
    }

    // Create new keys for encoding
    public_key = BN_new();
    secret_key = BN_new();
    BN_generate_prime_ex(public_key, AONT_BLOCK_SIZE * 8, true, NULL, NULL, NULL);
    BN_generate_prime_ex(secret_key, AONT_BLOCK_SIZE * 8, true, NULL, NULL, NULL);

    // Generate AES_KEY variable with secret key value
    unsigned char *tmp = (unsigned char *) BN_bn2hex(secret_key);
    secret_str = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));

    // Parse secret key to string format
    for (int i = 0; i < AONT_BLOCK_SIZE; i++){
        unsigned int firstHex = hex2int(tmp[i * 2]) << 4;
        secret_str[i] = firstHex + hex2int(tmp[i * 2 + 1]);
    }
    OPENSSL_free(tmp);
    // print_data("k'\t\t\t\t", secret_str, AONT_BLOCK_SIZE);

    // Parse public key to string format
    tmp = (unsigned char *) BN_bn2hex(public_key);
    public_str = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    for (int i = 0; i < AONT_BLOCK_SIZE; i++){
        unsigned int firstHex = hex2int(tmp[i * 2]) << 4;
        public_str[i] = firstHex + hex2int(tmp[i * 2 + 1]);
    }
    OPENSSL_free(tmp);

    // Prepare for making last secret block
    lastSecretBlock = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    // for(int i = 0; i < AONT_BLOCK_SIZE; i++)
    //     lastSecretBlock[i] = (unsigned char) (lastSecretBlock[i] ^ secret_str[i]);
    gf_object.multiply_region.w32(&gf_object, secret_str, lastSecretBlock, 1, AONT_BLOCK_SIZE, 1);

    // Initialize EVP variables
    aes_ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(aes_ctx);

    // Initialize vector
    vector = (void **) malloc(3 * sizeof(void *));

    // Clear public key and secret key with clear option
    BN_clear_free(public_key);
    BN_clear_free(secret_key);
}

AONT_Encode::~AONT_Encode(){
    OPENSSL_free(secret_str);
    OPENSSL_free(public_str);
    OPENSSL_free(lastSecretBlock);
    OPENSSL_free(vector);

    EVP_CIPHER_CTX_cleanup(aes_ctx);
    OPENSSL_free(aes_ctx);
    gf_free(&gf_object, 1);
}

bool AONT_Encode::encodeFile(const char *inputfile){
    int fread_ret;

    file_input = fopen(inputfile, "rb");

    if (file_input == NULL){
        cout << "Input file does not exist" << endl;
        return false;
    }

    fseek(file_input, 0, SEEK_END);
    fileSize = ftell(file_input);
    fseek(file_input, 0, SEEK_SET);
    int input_block_size = (fileSize % AONT_BLOCK_SIZE == 0 ? (fileSize / AONT_BLOCK_SIZE) : (fileSize / AONT_BLOCK_SIZE) + 1);

    // =======================================================================================
    // Encoding
    encodedfile = (unsigned char *) malloc((input_block_size + 2) * AONT_BLOCK_SIZE * sizeof(unsigned char));
    if (encodedfile == NULL) {
        cout << "Memory allocation fails" << endl;
        return false;
    }

    // First write it to cipher textfile
    unsigned char *public_str = this->getPublicKey();
    memcpy(encodedfile + 0, public_str, AONT_BLOCK_SIZE);

    // Get AONT_BLOCK_SIZE characters from textfile and encode
    for (int index = 0; index < input_block_size; index++){
        unsigned char *messageBlock = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
        if (index == input_block_size - 1){
            memset(messageBlock, 255, AONT_BLOCK_SIZE);
        }
        fread_ret = fread(messageBlock, AONT_BLOCK_SIZE, 1, file_input);
        
        if (fread_ret < 0){
            cout << "fread return: " << fread_ret << endl;
            return false;
        }
        bool encode_ret = this->encodeMessage(messageBlock, index);
        if (encode_ret == false){
            cout << "Encoded unsuccessfully" << endl;
            return false;
        }
        
        // Write this to cipher textfile
        unsigned char *encodedMessageBlock = this->getMessage();
        memcpy(encodedfile + (index + 1) * AONT_BLOCK_SIZE, encodedMessageBlock, AONT_BLOCK_SIZE);

        OPENSSL_free(messageBlock);
        OPENSSL_free(encodedMessageBlock);
    }
    
    // Finally write this at the end of textfile
    unsigned char *lastBlock = this->getLastEncodedBlock();
    memcpy(encodedfile + (input_block_size + 1) * AONT_BLOCK_SIZE, lastBlock, AONT_BLOCK_SIZE);
    
    fclose(file_input);

    return true;
}

unsigned char * AONT_Encode::getEncodedFile(){
    return encodedfile;
}

int AONT_Encode::getFileSize(){
    return fileSize;
}

int AONT_Encode::getEncodedFileSize(){
    int input_block_size = (fileSize % AONT_BLOCK_SIZE == 0 ? (fileSize / AONT_BLOCK_SIZE) : (fileSize / AONT_BLOCK_SIZE) + 1);
    return (input_block_size + 2) * AONT_BLOCK_SIZE;
}

int AONT_Encode::hex2int(char ch){
    if (ch >= '0' && ch <= '9')
        return ch - '0';
    if (ch >= 'A' && ch <= 'F')
        return ch - 'A' + 10;
    return -1;
}

// For printing message by hex string
void AONT_Encode::print_data(const char *title, const void* data, int len){
	printf("%s : ", title);
	const unsigned char *p = (const unsigned char *) data;
	
	for (int i = 0; i < len; i++)
		printf("%02X ", *p++);
	
	printf("\n");
}

unsigned char * AONT_Encode::getPublicKey(){
    // print_data("k_0\t\t\t\t", public_str, AONT_BLOCK_SIZE);
    return public_str;
}

unsigned char * AONT_Encode::getMessage(){
    // print_data("Encoded message", encodedMessageBlock, AES_BLOCK_SIZE);
    return encodedMessageBlock;
}

unsigned char * AONT_Encode::getLastEncodedBlock(){
    // print_data("m'_s+1\t\t\t\t", lastSecretBlock, AONT_BLOCK_SIZE);
    return lastSecretBlock;
}

bool AONT_Encode::encodeMessage(unsigned char *messageBlock, int blockIndex){
    // secretMessageBlock => messageBlock XOR AES(secret_key, blockNumber)
    // hashMessageBlock => MD5(public_key, secretMessageBlock XOR blockNumber)
    // lastSecretBlock => secret_key XOR hashMessageBlock (0 ... n - 1)
    // print_data("m_i\t\t\t\t", messageBlock, AONT_BLOCK_SIZE);
    
    // Initialization Vector: made by message block index from 0 to n - 1
    unsigned char *iv = (unsigned char *) calloc(AES_BLOCK_SIZE, sizeof(unsigned char));
    unsigned char *block_number = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));

    // block_number will be plain text of message
    int plain_number = blockIndex + 1;
    for (int i = AONT_BLOCK_SIZE - 1; i >= 0; i--){
        int temp = plain_number % 2;
        plain_number = plain_number / 2;
        block_number[i] = temp;
        if (plain_number == 0)
            break;
    }

    if (AONT_BLOCK_SIZE == 32) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_256_cbc(), NULL, secret_str, iv);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_128_cbc(), NULL, secret_str, iv);
    }

    // Encrypt message block with AES CBC and write data to encodedMessageBlock
    encodedMessageBlock = (unsigned char *) malloc(AONT_BLOCK_SIZE * sizeof(unsigned char));
    int return_length;
    int retstat = EVP_EncryptUpdate(aes_ctx, encodedMessageBlock, &return_length, block_number, AONT_BLOCK_SIZE);
    if (!retstat || return_length != AONT_BLOCK_SIZE) {
        cout << "EVP encrypt error" << endl;
		return false;
	}
    OPENSSL_free(iv);
    
    // gf_object.multiply_region.w32(&gf_object, messageBlock, encodedMessageBlock, 1, AONT_BLOCK_SIZE, 1);
    vector[0] = messageBlock;
    vector[1] = encodedMessageBlock;
    vector[2] = encodedMessageBlock;
    xor_gen_sse(3, AONT_BLOCK_SIZE, vector);

    // print_data("m'_i = m_i XOR AES(k', i)\t", encodedMessageBlock, AONT_BLOCK_SIZE);

    // Hash the message block
    unsigned char *x_or = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    memcpy(x_or, encodedMessageBlock, AONT_BLOCK_SIZE);
    // gf_object.multiply_region.w32(&gf_object, block_number, x_or, 1, AONT_BLOCK_SIZE, 1);
    vector[0] = block_number;
    vector[1] = x_or;
    vector[2] = x_or;
    xor_gen_sse(3, AONT_BLOCK_SIZE, vector);
    OPENSSL_free(block_number);
    
    // print_data("m'_i XOR i\t", x_or, AONT_BLOCK_SIZE);

    unsigned char *hash_ret = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    unsigned char *hash_message = (unsigned char *) calloc(AONT_BLOCK_SIZE * 2, sizeof(unsigned char));

    memcpy(hash_message, public_str, AONT_BLOCK_SIZE);
    memcpy(hash_message + AONT_BLOCK_SIZE, x_or, AONT_BLOCK_SIZE);

    if (AONT_BLOCK_SIZE == 32) {
        EVP_Digest(hash_message, AONT_BLOCK_SIZE * 2, hash_ret, NULL, EVP_sha256(), NULL);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_Digest(hash_message, AONT_BLOCK_SIZE * 2, hash_ret, NULL, EVP_md5(), NULL);
    }
    // print_data("h_i = SHA256(k_0, m'_i XOR i)\t", hash_ret, AONT_BLOCK_SIZE);
    OPENSSL_free(x_or);
    OPENSSL_free(hash_message);

    // XOR hash_ret to lastSecretBlock
    // gf_object.multiply_region.w32(&gf_object, hash_ret, lastSecretBlock, 1, AONT_BLOCK_SIZE, 1);
    vector[0] = hash_ret;
    vector[1] = lastSecretBlock;
    vector[2] = lastSecretBlock;
    xor_gen_sse(3, AONT_BLOCK_SIZE, vector);

    OPENSSL_free(hash_ret);
    return true;
}

// ====================================================================================================

// void *encodeBlock(unsigned char *messageBlock, int blockIndex, int thread_id){
//     // TODO
//     pthread_exit(NULL);
// }


// AONT_Encode_MultiThread::~AONT_Encode_MultiThread(){
// }

// bool AONT_Encode_MultiThread::encodeFile(const char *inputfile){
//     // TODO
//     return true;
// }

// ====================================================================================================

CAONT_Encode::CAONT_Encode(short key_size){
    AONT_BLOCK_SIZE = key_size;

    // Initialize EVP_MD for MGF1
    digest = EVP_sha1();

    // Initialize EVP variables
    aes_ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(aes_ctx);

    // Initiailze GF-Complete variables
    if (!gf_init_easy(&gf_object, 8)) {
        fprintf(stderr, "Error: bad gf specification\n");
        exit(1);
    }

    // Initialize vector
    vector = (void **) malloc(3 * sizeof(void *));

    encodeComplete = false;
}

CAONT_Encode::~CAONT_Encode(){
    OPENSSL_free(encodedfile);
    OPENSSL_free(vector);

    gf_free(&gf_object, 1);
}

// For printing message by hex string
void CAONT_Encode::print_data(const char *title, const void* data, int len){
	printf("%s : ", title);
	const unsigned char *p = (const unsigned char *) data;
	
	for (int i = 0; i < len; i++)
		printf("%02X ", *p++);
	
	printf("\n");
}

int CAONT_Encode::getFileSize(){
    return realFileSize;
}

int CAONT_Encode::getEncodedFileSize(){
    return fileSize;
}

unsigned char * CAONT_Encode::getEncodedFile(){
    return encodedfile;
}

void *XOR_thread(void *input){
    struct XOR_struct *xor_segment = (struct XOR_struct *) input;

    gf_t gf_object;
    // Initiailze GF-Complete variables
    if (!gf_init_easy(&gf_object, 8)) {
        fprintf(stderr, "Error: bad gf specification\n");
        exit(1);
    }

    gf_object.multiply_region.w32(&gf_object, xor_segment->input_segment, xor_segment->xor_segment, 1, xor_segment->segment_size, 1);
    pthread_exit(xor_segment);
}

bool CAONT_Encode::readFromFile(const char *inputfile){
    file_input = fopen(inputfile, "rb");

    if (file_input == NULL){
        cout << "Input file does not exist" << endl;
        return false;
    }

    fseek(file_input, 0, SEEK_END);
    fileSize = ftell(file_input);
    fseek(file_input, 0, SEEK_SET);
    int input_block_size = (fileSize % AONT_BLOCK_SIZE == 0 ? (fileSize / AONT_BLOCK_SIZE) : (fileSize / AONT_BLOCK_SIZE) + 1);
    realFileSize = fileSize;

    // =======================================================================================
    originalfile = (unsigned char *) calloc(input_block_size * AONT_BLOCK_SIZE, sizeof(unsigned char));
    if (originalfile == NULL) {
        cout << "Memory allocation fails" << endl;
        return false;
    }

    // Copy whole file to memory
    int fread_ret = fread(originalfile, fileSize, sizeof(unsigned char), file_input);
    if (fread_ret < 0){
        cout << "fread return: " << fread_ret << endl;
        return false;
    }

    // cout << "X: " << endl;
    // for (int i = 0; i < fileSize; i++)
    //     cout << (unsigned int) originalfile[i] << " ";
    // cout << endl << endl;

    fclose(file_input);
    fileSize = input_block_size * AONT_BLOCK_SIZE;
    return true;
}

bool CAONT_Encode::encodeFile(int thread_number){
    // Construct constant block with same size as input
    // C
    const_block = (unsigned char *) calloc(fileSize, sizeof(unsigned char));
    const_message = (unsigned char *) calloc(fileSize, sizeof(unsigned char));

    // Create hashed value derived from input
    // h
    if (AONT_BLOCK_SIZE == 32) {
        EVP_Digest(originalfile, fileSize, input_hash, NULL, EVP_sha256(), NULL);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_Digest(originalfile, fileSize, input_hash, NULL, EVP_md5(), NULL);
    }

    unsigned char *iv = (unsigned char *) calloc(AES_BLOCK_SIZE, sizeof(unsigned char));
    // EVP_aes_256_ecb() < EVP_aes_256_cbc()
    if (AONT_BLOCK_SIZE == 32) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_256_cbc(), NULL, input_hash, iv);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_128_cbc(), NULL, input_hash, iv);
    }

    int return_length;
    int retstat = EVP_EncryptUpdate(aes_ctx, const_block, &return_length, const_message, fileSize);
    if (!retstat || return_length != fileSize) {
        cout << "EVP encrypt error" << endl;
		return false;
	}
    OPENSSL_free(const_message);

    // Construct mask block by encrpyting constant block by hash value
    // G(h)
    encodedfile = (unsigned char *) calloc(fileSize, sizeof(unsigned char));
    memset(iv, 0, AES_BLOCK_SIZE);
    // EVP_aes_256_ecb() < EVP_aes_256_cbc()
    if (AONT_BLOCK_SIZE == 32) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_256_cbc(), NULL, input_hash, iv);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_128_cbc(), NULL, input_hash, iv);
    }
    retstat = EVP_EncryptUpdate(aes_ctx, encodedfile, &return_length, const_block, fileSize);
    if (!retstat || return_length != fileSize) {
        cout << "EVP encrypt error" << endl;
		return false;
	}

    OPENSSL_free(const_block);
    OPENSSL_free(iv);

    // XOR with input message to form head part
    // Y = X XOR G(h)
    if (thread_number > 1){
        total_thread = thread_number;
        threads = (pthread_t *) malloc(total_thread * sizeof(pthread_t));
        message_struct = (struct XOR_struct *) malloc(total_thread * sizeof(struct XOR_struct));

        for (int i = 0; i < total_thread; i++) {
            int start_segment = i * fileSize / total_thread;
            int end_segment = ((i + 1) * fileSize / total_thread > fileSize) ? fileSize : (i + 1) * fileSize / total_thread;

            unsigned char *segment = (unsigned char *) malloc((end_segment - start_segment) * sizeof(unsigned char));
            memcpy(segment, originalfile + start_segment, (end_segment - start_segment));

            unsigned char *output_segment = (unsigned char *) malloc((end_segment - start_segment) * sizeof(unsigned char));
            memcpy(output_segment, encodedfile + start_segment, (end_segment - start_segment));

            message_struct[i].thread_id = i;
            message_struct[i].segment_size = end_segment - start_segment;
            message_struct[i].input_segment = segment;
            message_struct[i].xor_segment = output_segment;
            
            pthread_create(&threads[i], NULL, XOR_thread, (void *) &message_struct[i]);
        }

        for (int i = 0; i < total_thread; i++) {
            struct XOR_struct *output = nullptr;
            pthread_join(threads[i], (void **) &output);
            memcpy(encodedfile + output->thread_id * fileSize / total_thread, output->xor_segment, output->segment_size);
            OPENSSL_free(output->input_segment);
            OPENSSL_free(output->xor_segment);
        }

        OPENSSL_free(message_struct);
        OPENSSL_free(threads);
    }
    else{
        // gf_object.multiply_region.w32(&gf_object, originalfile, encodedfile, 1, fileSize, 1);
        vector[0] = originalfile;
        vector[1] = encodedfile;
        vector[2] = encodedfile;
        xor_gen_sse(3, fileSize, vector);
    }

    // free memory
    OPENSSL_free(originalfile);
    EVP_CIPHER_CTX_cleanup(aes_ctx);
    EVP_CIPHER_CTX_free(aes_ctx);
    
    // Create hashed value derived from encoded message
    // H(Y)
    if (AONT_BLOCK_SIZE == 32) {
        EVP_Digest(encodedfile, fileSize, encoded_hash, NULL, EVP_sha256(), NULL);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_Digest(encodedfile, fileSize, encoded_hash, NULL, EVP_md5(), NULL);
    }

    // XOR with input hash value to form head part
    // t = h XOR H(Y)
    // gf_object.multiply_region.w32(&gf_object, input_hash, encoded_hash, 1, AONT_BLOCK_SIZE, 1);

    vector[0] = input_hash;
    vector[1] = encoded_hash;
    vector[2] = encoded_hash;
    xor_gen_sse(3, AONT_BLOCK_SIZE, vector);

    encodeComplete = true;
    return true;
}

bool CAONT_Encode::writetofile(const char *encodefile){
    if (encodeComplete == false){
        cout << "Not yet encoded file" << endl;
        return false;
    }

    FILE *file_encode = fopen(encodefile, "wb+");
    if (file_encode == NULL){
        cout << "Encode file opens error" << endl;
        return false;
    }

    fwrite(encodedfile, fileSize, sizeof(unsigned char), file_encode);
    fwrite(encoded_hash, AONT_BLOCK_SIZE, sizeof(unsigned char), file_encode);

    fclose(file_encode);

    return true;
}

// ====================================================================================================