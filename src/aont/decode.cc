#include <string.h>

#include <iostream>
#include <sstream>
#include <iomanip>

#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/md5.h>
#include <openssl/crypto.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>

#include "aont.hh"
#include "decode.hh"

using namespace std;

AONT_Decode::AONT_Decode(short key_size){
    AONT_BLOCK_SIZE = key_size;

    public_key_flag = false;
    secret_key = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));

    // Initialize EVP variables
    aes_ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(aes_ctx);

    // Initiailze GF-Complete variables
    if (!gf_init_easy(&gf_object, 8)) {
        fprintf(stderr, "Error: bad gf specification\n");
        exit(1);
    }

    // Initialize vector
    vector = (void **) malloc(3 * sizeof(void *));
}

AONT_Decode::~AONT_Decode(){
    OPENSSL_free(public_key);
    OPENSSL_free(secret_key);
    OPENSSL_free(vector);

    EVP_CIPHER_CTX_cleanup(aes_ctx);
    OPENSSL_free(aes_ctx);
    gf_free(&gf_object, 1);
}

bool AONT_Decode::decodeFile(const char *encodefile, const char *decodefile){
    int fread_ret;
    // =======================================================================================
    // Decoding

    file_encode = fopen(encodefile, "rb");
    file_decode = fopen(decodefile, "wb+");

    fseek(file_encode, 0, SEEK_END);
    int encoded_file_size = ftell(file_encode);
    fseek(file_encode, 0, SEEK_SET);
    int message_block_size = (encoded_file_size / AONT_BLOCK_SIZE) - 2;
    // cout << "Encoded file size: " << encoded_file_size << endl;
    // cout << "Encoded file block size: " << message_block_size << endl;

    if (file_encode == NULL){
        cout << "Encoded file does not exist" << endl;
        return false;
    }

    // Read first AONT_BLOCK_SIZE and set it to public key
    unsigned char *public_key = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    fread_ret = fread(public_key, AONT_BLOCK_SIZE, 1, file_encode);
    if (fread_ret < 0){
        cout << "fread return: " << fread_ret << endl;
        return false;
    }
    this->setPublicKey(public_key);

    
    for (int index = 0; index < message_block_size; index++){
        unsigned char *cipherBlock = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
        fread_ret = fread(cipherBlock, AONT_BLOCK_SIZE, 1, file_encode);
        if (fread_ret < 0){
            cout << "fread return: " << fread_ret << endl;
        }
        // Get AONT_BLOCK_SIZE characters from cipher textfile and decode
        bool decode_ret = this->decodeSecretKey(cipherBlock, index);
        if (decode_ret == false){
            cout << "Added block unsuccessfully" << endl;
            exit(1);
        }
        OPENSSL_free(cipherBlock);
    }

    // Finally set the last AONT_BLOCK_SIZE
    unsigned char *lastSecretBlock = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    fread_ret = fread(lastSecretBlock, AONT_BLOCK_SIZE, 1, file_encode);
    if (fread_ret < 0){
        cout << "fread return: " << fread_ret << endl;
        return false;
    }
    // print_data("m'_s+1\t\t\t\t", lastSecretBlock, AONT_BLOCK_SIZE);
    this->setLastEncodedBlock(lastSecretBlock);
    OPENSSL_free(lastSecretBlock);

    // Set file pointer after public key
    fseek(file_encode, AONT_BLOCK_SIZE, SEEK_SET);

    for (int index = 0; index < message_block_size; index++){
        // Read from textfile file again and decode message
        unsigned char *cipherBlock = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
        fread_ret = fread(cipherBlock, AONT_BLOCK_SIZE, 1, file_encode);
        if (fread_ret < 0){
            cout << "fread return: " << fread_ret << endl;
        }
        unsigned char *plainBlock = this->decodeMessage(cipherBlock, index);
        if (plainBlock == nullptr) {
            exit(1);
        }
        if (index < message_block_size - 1)
            fwrite(plainBlock, AONT_BLOCK_SIZE, 1, file_decode);
        else{
            int realBlockSize = 0;
            for (int i = AONT_BLOCK_SIZE - 1; i >= 0; i--){
                if (((unsigned int) plainBlock[i]) != 255){
                    realBlockSize = i;
                    break;
                }
            }
            fwrite(plainBlock, realBlockSize + 1, 1, file_decode);
        }

        OPENSSL_free(cipherBlock);
        OPENSSL_free(plainBlock);
    }

    fclose(file_encode);
    fclose(file_decode);

    return true;
}

// For printing message by hex string
void AONT_Decode::print_data(const char *title, const void* data, int len){
	printf("%s : ", title);
	const unsigned char *p = (const unsigned char *) data;
	
	for (int i = 0; i < len; i++)
		printf("%02X ", *p++);
	
	printf("\n");
}

void AONT_Decode::setPublicKey(unsigned char *k){
    public_key = k;
    public_key_flag = true;
    // print_data("k_0\t\t\t\t", public_key, AONT_BLOCK_SIZE);
}

void AONT_Decode::setLastEncodedBlock(unsigned char *lastSecretBlock){
    // for(int i = 0; i < AONT_BLOCK_SIZE; i++)
    //     secret_key[i] = (unsigned char) (secret_key[i] ^ lastSecretBlock[i]);
    gf_object.multiply_region.w32(&gf_object, lastSecretBlock, secret_key, 1, AONT_BLOCK_SIZE, 1);
}

bool AONT_Decode::decodeSecretKey(unsigned char *secretMessageBlock, int blockIndex){
    // hashMessageBlock => MD5(public_key, secretMessageBlock XOR blockNumber)
    // secret_key => lastSecretBlock XOR hashMessageBlock (0 ... n - 1)
    // messageBlock => secretMessageBlock XOR AES(secret_key, blockNumber)

    // Not yet set public key
    if (public_key_flag == false)
        return false;

    // Hash the message block
    unsigned char *blocknumber = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
	int plain_number = blockIndex + 1;
    for (int i = AONT_BLOCK_SIZE - 1; i >= 0; i--){
        int temp = plain_number % 2;
        plain_number = plain_number / 2;
        blocknumber[i] = temp;
        if (plain_number == 0)
            break;
    }

    unsigned char *x_or = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    memcpy(x_or, blocknumber, AONT_BLOCK_SIZE);
    // for(int i = 0; i < AONT_BLOCK_SIZE; i++)
    //     x_or[i] = (unsigned char) (secretMessageBlock[i] ^ blocknumber[i]);
    gf_object.multiply_region.w32(&gf_object, secretMessageBlock, x_or, 1, AONT_BLOCK_SIZE, 1);

    // print_data("m'_i XOR i\t", x_or, AONT_BLOCK_SIZE);

    unsigned char *hash_ret = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    unsigned char *hash_message = (unsigned char *) calloc(AONT_BLOCK_SIZE * 2, sizeof(unsigned char));

    memcpy(hash_message, public_key, AONT_BLOCK_SIZE);
    memcpy(hash_message + AONT_BLOCK_SIZE, x_or, AONT_BLOCK_SIZE);

    if (AONT_BLOCK_SIZE == 32) {
        EVP_Digest(hash_message, AONT_BLOCK_SIZE * 2, hash_ret, NULL, EVP_sha256(), NULL);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_Digest(hash_message, AONT_BLOCK_SIZE * 2, hash_ret, NULL, EVP_md5(), NULL);
    }
    // print_data("h_i = SHA256(k_0, m'_i XOR i)\t", hash_ret, AONT_BLOCK_SIZE);

    // XOR hash_ret to lastSecretBlock
    // for(int i = 0; i < AONT_BLOCK_SIZE; i++)
    //     secret_key[i] = (unsigned char) (secret_key[i] ^ hash_ret[i]);
    gf_object.multiply_region.w32(&gf_object, hash_ret, secret_key, 1, AONT_BLOCK_SIZE, 1);

    OPENSSL_free(x_or);
    OPENSSL_free(hash_ret);
    OPENSSL_free(blocknumber);
    OPENSSL_free(hash_message);
    return true;
}

unsigned char * AONT_Decode::decodeMessage(unsigned char *secretMessageBlock, int blockIndex){
    // Initialization Vector: made by message block index from 0 to n - 1
    unsigned char *iv = (unsigned char *) calloc(AES_BLOCK_SIZE, sizeof(unsigned char));
    unsigned char *block_number = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));

    // block_number will be plain text of message
    int plain_number = blockIndex + 1;
    for (int i = AONT_BLOCK_SIZE - 1; i >= 0; i--){
        int temp = plain_number % 2;
        plain_number = plain_number / 2;
        block_number[i] = temp;
        if (plain_number == 0)
            break;
    }

    if (AONT_BLOCK_SIZE == 32) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_256_cbc(), NULL, secret_key, iv);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_128_cbc(), NULL, secret_key, iv);
    }

    // Decrypt message block with AES CBC and write data to encodedMessageBlock
    decodedMessageBlock = (unsigned char *) malloc(AONT_BLOCK_SIZE * sizeof(unsigned char));
    int return_length;
    int retstat = EVP_EncryptUpdate(aes_ctx, decodedMessageBlock, &return_length, block_number, AONT_BLOCK_SIZE);
    if (!retstat || return_length != AONT_BLOCK_SIZE) {
        cout << "EVP decrypt error" << endl;
		return nullptr;
	}
    OPENSSL_free(iv);
    OPENSSL_free(block_number);

    // gf_object.multiply_region.w32(&gf_object, secretMessageBlock, decodedMessageBlock, 1, AONT_BLOCK_SIZE, 1);
    vector[0] = secretMessageBlock;
    vector[1] = decodedMessageBlock;
    vector[2] = decodedMessageBlock;
    xor_gen_sse(3, AONT_BLOCK_SIZE, vector);

    // print_data("m_i = m'_i XOR AES(k', i)\t", decodedMessageBlock, AONT_BLOCK_SIZE);

    return decodedMessageBlock;
}

CAONT_Decode::CAONT_Decode(short key_size){
    AONT_BLOCK_SIZE = key_size;

    // Initialize EVP_MD for MGF1
    digest = EVP_sha1();

    // Initialize EVP variables
    aes_ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(aes_ctx);

    // Allocate memory to hash array
    input_hash = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));
    encoded_hash = (unsigned char *) calloc(AONT_BLOCK_SIZE, sizeof(unsigned char));

    // Initiailze GF-Complete variables
    if (!gf_init_easy(&gf_object, 8)) {
        fprintf(stderr, "Error: bad gf specification\n");
        exit(1);
    }

    // Initialize vector
    vector = (void **) malloc(3 * sizeof(void *));

    decodeComplete = false;
}

CAONT_Decode::~CAONT_Decode(){
    // free memory
    OPENSSL_free(encodedfile);
    OPENSSL_free(originalfile);

    OPENSSL_free(input_hash);
    OPENSSL_free(encoded_hash);

    OPENSSL_free(vector);
    
    gf_free(&gf_object, 1);
}

// For printing message by hex string
void CAONT_Decode::print_data(const char *title, const void* data, int len){
	printf("%s : ", title);
	const unsigned char *p = (const unsigned char *) data;
	
	for (int i = 0; i < len; i++)
		printf("%02X ", *p++);
	
	printf("\n");
}

bool CAONT_Decode::decodeFile(const char *encodefile){
    int fread_ret;

    file_encode = fopen(encodefile, "rb");

    if (file_encode == NULL){
        cout << "Input file does not exist" << endl;
        return false;
    }

    fseek(file_encode, 0, SEEK_END);
    fileSize = ftell(file_encode);
    fseek(file_encode, 0, SEEK_SET);
    fileSize = fileSize - AONT_BLOCK_SIZE;

    // Read file and separate Y and t
    encodedfile = (unsigned char *) calloc(fileSize, sizeof(unsigned char));
    if (encodedfile == NULL) {
        cout << "Memory allocation fails" << endl;
        return false;
    }

    // Copy first part to memory
    fread_ret = fread(encodedfile, fileSize, sizeof(unsigned char), file_encode);
    if (fread_ret < 0){
        cout << "fread return: " << fread_ret << endl;
        return false;
    }

    fread_ret = fread(encoded_hash, AONT_BLOCK_SIZE, sizeof(unsigned char), file_encode);
    if (fread_ret < 0){
        cout << "fread return: " << fread_ret << endl;
        return false;
    }
    
    fclose(file_encode);

    // Create H(Y)
    if (AONT_BLOCK_SIZE == 32) {
        EVP_Digest(encodedfile, fileSize, input_hash, NULL, EVP_sha256(), NULL);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_Digest(encodedfile, fileSize, input_hash, NULL, EVP_md5(), NULL);
    }

    // h = t XOR H(Y)
    // gf_object.multiply_region.w32(&gf_object, encoded_hash, input_hash, 1, AONT_BLOCK_SIZE, 1);
    vector[0] = encoded_hash;
    vector[1] = input_hash;
    vector[2] = input_hash;
    xor_gen_sse(3, AONT_BLOCK_SIZE, vector);

    // C
    const_block = (unsigned char *) calloc(fileSize, sizeof(unsigned char));
    unsigned char *iv = (unsigned char *) calloc(AES_BLOCK_SIZE, sizeof(unsigned char));
    const_message = (unsigned char *) calloc(fileSize, sizeof(unsigned char));

    if (AONT_BLOCK_SIZE == 32) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_256_cbc(), NULL, input_hash, iv);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_128_cbc(), NULL, input_hash, iv);
    }
    int return_length;
    int retstat = EVP_EncryptUpdate(aes_ctx, const_block, &return_length, const_message, fileSize);
    if (!retstat || return_length != fileSize) {
        cout << "EVP encrypt error" << endl;
		return false;
	}
    OPENSSL_free(const_message);

    // G(h)
    originalfile = (unsigned char *) calloc(fileSize, sizeof(unsigned char));
    memset(iv, 0, AES_BLOCK_SIZE);
    if (AONT_BLOCK_SIZE == 32) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_256_cbc(), NULL, input_hash, iv);
    }
    else if (AONT_BLOCK_SIZE == 16) {
        EVP_EncryptInit_ex(aes_ctx, EVP_aes_128_cbc(), NULL, input_hash, iv);
    }
    
    // AES_cbc_encrypt(const_block, originalfile, fileSize, &hash_key, iv, AES_ENCRYPT);
    retstat = EVP_EncryptUpdate(aes_ctx, originalfile, &return_length, const_block, fileSize);
    if (!retstat || return_length != fileSize) {
        cout << "EVP encrypt error" << endl;
		return false;
	} 

    OPENSSL_free(iv);
    OPENSSL_free(const_block);
    EVP_CIPHER_CTX_cleanup(aes_ctx);
    EVP_CIPHER_CTX_free(aes_ctx);

    // XOR with input message to form head part
    // X = Y XOR G(h)
    // gf_object.multiply_region.w32(&gf_object, encodedfile, originalfile, 1, fileSize, 1);
    vector[0] = encodedfile;
    vector[1] = originalfile;
    vector[2] = originalfile;
    xor_gen_sse(3, fileSize, vector);

    // Check real message size without padding
    realdatasize = fileSize;
    for (int i = fileSize - 1; i >= fileSize - AONT_BLOCK_SIZE; i--){
        if (((unsigned int) originalfile[i]) != 0){
            realdatasize = i + 1;
            break;
        }
    }

    // cout << "X: " << endl;
    // for (int i = 0; i < realdatasize; i++)
    //     cout << (unsigned int) originalfile[i] << " ";
    // cout << endl << endl;

    decodeComplete = true;
    return true;
}

bool CAONT_Decode::writetofile(const char *decodefile){
    if (decodeComplete == false){
        cout << "Not yet decoded the encoded file" << endl;
        return false;
    }

    FILE *file_decode = fopen(decodefile, "wb+");

    fwrite(originalfile, realdatasize, 1, file_decode);

    fclose(file_decode);
    return true;
}