#ifndef __AONT_HH__
#define __AONT_HH__

extern "C" {
    #include <isa-l/erasure_code.h>
    #include <isa-l/test.h>
    #include <isa-l/raid.h>
    #include <gf_complete.h>
}

struct XOR_struct {
    int thread_id;
    int segment_size;
    unsigned char *input_segment, *xor_segment;
};

void *XOR_thread(void *input);

#endif
