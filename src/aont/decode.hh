#ifndef __DECODE_HH__
#define __DECODE_HH__

#include <openssl/bn.h>
#include <openssl/bio.h>
#include <openssl/md5.h>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>

#include "aont.hh"

class AONT_Decode {
    public:
        AONT_Decode(short key_size);
        virtual ~AONT_Decode();

        virtual void print_data(const char *title, const void* data, int len);

        virtual bool decodeFile(const char *encodefile, const char *decodefile);

        virtual void setPublicKey(unsigned char *k);
        virtual void setLastEncodedBlock(unsigned char *lastSecretBlock);

        virtual bool decodeSecretKey(unsigned char *secretMessageBlock, int blockIndex);
        virtual unsigned char * decodeMessage(unsigned char *secretMessageBlock, int blockIndex);
    
    protected:
        // Metadata
        short AONT_BLOCK_SIZE;
        int fileSize;
        bool public_key_flag;

        // FILE pointer
        FILE *file_encode, *file_decode;

        // OpenSSL variables
        EVP_CIPHER_CTX *aes_ctx;

        // GF-Complete variables
        gf_t gf_object;

        // Message data
        unsigned char *decodedMessageBlock;

        // Key strings
        unsigned char *public_key, *secret_key;

        // ISA-L XOR vector
        void **vector;
};

class CAONT_Decode {
    public:
        CAONT_Decode(short key_size);
        virtual ~CAONT_Decode();

        virtual void print_data(const char *title, const void* data, int len);

        virtual bool decodeFile(const char *encodefile);
        virtual bool writetofile(const char *decodefile);
    
    protected:
        // Metadata
        short AONT_BLOCK_SIZE;
        bool decodeComplete;
        int fileSize, realdatasize;

        // FILE pointer
        FILE *file_encode, *file_decode;

        // OpenSSL variables
        const EVP_MD *digest;

        // OpenSSL EVP variables
        EVP_CIPHER_CTX *aes_ctx;

        // GF-Complete variables
        gf_t gf_object;

        // Message data
        unsigned char *const_message;
        unsigned char *const_block, *mask_block;
        unsigned char *input_hash, *encoded_hash;
        unsigned char *encodedfile, *originalfile;

        // ISA-L XOR vector
        void **vector;
};
#endif