#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include "encode.hh"
#include "decode.hh"
#include "aont.hh"
#include "rscode.hh"

using namespace std;

int main(int argc, char **argv){
    struct perf startp, stopp;

    if (argc < 6) {
        cout << "Usage: ./aont [--encode / --decode] INPUT_FILE OUTPUT_FILE [AONT: 1 / CAONT: 2] [SECURE_LEVEL: HIGH / LOW]" << endl;
        exit(1);
    }

    short key_size = 0;
    if (strcmp(argv[5], "HIGH") == 0)
        key_size = 32;
    else
        key_size = 16;

    if (strcmp(argv[1], "--encode") == 0) {
        if (strcmp(argv[4], "1") == 0) {
            perf_start(&startp);

            AONT_Encode *encoder = new AONT_Encode(key_size);
            bool encoder_ret = encoder->encodeFile(argv[2]);

            if (encoder_ret == false) {
                cout << "Encoded unsuccessfully" << endl;
                exit(1);
            }

            perf_stop(&stopp);
            perf_print(stopp, startp, (long long) encoder->getFileSize());

            FILE *file_encode = fopen(argv[3], "wb+");
            unsigned char *encodedFile = encoder->getEncodedFile();
            fwrite(encodedFile, encoder->getEncodedFileSize(), 1, file_encode);
            fclose(file_encode);
            free(encodedFile);

            delete encoder;
        }
        else if (strcmp(argv[4], "2") == 0) {
            CAONT_Encode *encoder = new CAONT_Encode(key_size);
            bool readFile_ret = encoder->readFromFile(argv[2]);
            if (readFile_ret == false) {
                cout << "Read file unsuccessfully" << endl;
                exit(1);
            }

            perf_start(&startp);
            bool encoder_ret = encoder->encodeFile(1);

            if (encoder_ret == false) {
                cout << "Encoded unsuccessfully" << endl;
                exit(1);
            }

            perf_stop(&stopp);
            perf_print(stopp, startp, (long long) encoder->getFileSize());

            encoder->writetofile(argv[3]);

            delete encoder;
        }
    }

    else if (strcmp(argv[1], "--decode") == 0) {
        if (strcmp(argv[4], "1") == 0) {
            // Initialize AONT_Decode object
            AONT_Decode *decoder = new AONT_Decode(key_size);
            bool decoder_ret = decoder->decodeFile(argv[2], argv[3]);
            if (decoder_ret == false) {
                cout << "Decoded unsuccessfully" << endl;
                exit(1);
            }
            delete decoder;
        }
        else if (strcmp(argv[4], "2") == 0) {
            CAONT_Decode *decoder = new CAONT_Decode(key_size);
            bool decoder_ret = decoder->decodeFile(argv[2]);
            if (decoder_ret == false) {
                cout << "Decoded unsuccessfully" << endl;
                exit(1);
            }

            decoder->writetofile(argv[3]);

            delete decoder;
        }
    }

    return 0;
}
