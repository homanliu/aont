#ifndef __ENCODE_HH__
#define __ENCODE_HH__

#include <pthread.h>

#include <openssl/bn.h>
#include <openssl/bio.h>
#include <openssl/md5.h>
#include <openssl/aes.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <openssl/evp.h>

#include "aont.hh"

class AONT_Encode {
    public:
        AONT_Encode(short key_size);
        virtual ~AONT_Encode();

        virtual bool encodeFile(const char *inputfile);
        virtual int getFileSize();
        virtual int getEncodedFileSize();

        virtual int hex2int(char ch);
        virtual void print_data(const char *title, const void* data, int len);
        virtual unsigned char * getMessage();
        virtual unsigned char * getPublicKey();
        virtual unsigned char * getLastEncodedBlock();
        virtual unsigned char * getEncodedFile();

        virtual bool encodeMessage(unsigned char *messageBlock, int blockIndex);
    
    protected:
        // Metadata
        short AONT_BLOCK_SIZE;
        int fileSize;

        // FILE pointer
        FILE *file_input, *file_encode;

        // OpenSSL variables
        BIGNUM *public_key, *secret_key;
        EVP_CIPHER_CTX *aes_ctx;

        // GF-Complete variables
        gf_t gf_object;

        // Message data
        unsigned char *encodedMessageBlock;
        unsigned char *lastSecretBlock;
        unsigned char *encodedfile;

        // Key strings
        unsigned char *secret_str, *public_str;

        // ISA-L XOR vector
        void **vector;
};

class AONT_Encode_MultiThread : public AONT_Encode {
    // public:
    //     AONT_Encode_MultiThread(int threads) : AONT_Encode(int key_size){
    //         number_of_thread = threads;
    //         *encodedMessageBlock = (unsigned char *) malloc(number_of_thread * sizeof(unsigned char *));
    //         encodingThread = (pthread_t *) malloc(number_of_thread * sizeof(pthread_t));
    //         pthread_mutex_init(lastSecretLock, NULL);
    //     }
    //     virtual ~AONT_Encode_MultiThread();

    //     virtual bool encodeFile(const char *inputfile);
    
    // private:
    //     int number_of_thread;
    //     unsigned char **encodedMessageBlock;
    //     pthread_t *encodingThread;
    //     pthread_mutex_t *lastSecretLock;
};

class CAONT_Encode {
    public:
        CAONT_Encode(short key_size);
        virtual ~CAONT_Encode();

        virtual void print_data(const char *title, const void* data, int len);

        virtual int getFileSize();
        virtual int getEncodedFileSize();
        virtual unsigned char * getEncodedFile();

        virtual bool readFromFile(const char *inputfile);
        virtual bool encodeFile(int thread_number);
        virtual bool writetofile(const char *encodefile);
    
    protected:
        // Metadata
        short AONT_BLOCK_SIZE;
        bool encodeComplete;
        int fileSize, realFileSize;

        // FILE pointer
        FILE *file_input, *file_encode;

        // OpenSSL variables
        const EVP_MD *digest;
        SHA256_CTX hash_input, hash_encrypt;
        AES_KEY hash_key;

        // OpenSSL EVP variables
        EVP_CIPHER_CTX *aes_ctx;

        // GF-Complete variables
        gf_t gf_object;

        // Message data
        unsigned char *const_message;
        unsigned char *const_block, *mask_block;
        unsigned char input_hash[32], encoded_hash[32];
        unsigned char *encodedfile, *originalfile;

        // Threading variables
        pthread_t *threads;
        int total_thread;
        struct XOR_struct *message_struct;

        // ISA-L XOR vector
        void **vector;
};
#endif