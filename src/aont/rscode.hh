#ifndef __RSCODE_HH__
#define __RSCODE_HH__

int demo(int argc, char *argv[]);

class RSCode {
    public:
        RSCode(int n, int k);
        virtual ~RSCode();

        virtual int getNumChunksPerNode();
        virtual int getChunkSize(unsigned long int dataSize);
        virtual int getNumDataChunks();
        virtual int getNumCodeChunks();
        virtual int getMatrixSize();

        virtual bool encode(unsigned char *data, unsigned long int dataSize, unsigned char *code, unsigned long int codeSize, unsigned char *matrix);
        virtual bool decode(unsigned char *input, unsigned long int inputSize, unsigned char *decodeData, unsigned long int &decodeSize, int *chunkIndices, int numChunks, unsigned char *encodingMatrix);
    
    protected:
        // Metadata
        int _n, _k;

        // Message data
        unsigned char *encode_matrix, *decode_matrix, *invert_matrix, *galois_table;
};

#endif