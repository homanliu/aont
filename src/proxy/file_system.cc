#include <dirent.h>  // DIR
#include <errno.h>
#include <fcntl.h>   // creat()
#include <stdlib.h>  // exit()
#include <stdio.h>   // printf()
#include <string.h>  // strlen()
#include <unistd.h>  // getuid(), lstat(), unlink()

#include <linux/limits.h>  // PATH_MAX
#include <sys/stat.h>      // creat(), lstat(), mkdir()
#include <sys/types.h>     // getuid(), lstat(), mkdir()

#define FUSE_USE_VERSION 26  // (see https://stackoverflow.com/a/2299336)
#include <fuse.h>

#include <glog/logging.h> // logging library

#include "cache.hh"

Cache *cache;  // handle the temporary cached file data, e.g., file yet completely read/written

const char *metadir = "metadata";  // for store the file records locally
char parentdir[PATH_MAX];

struct file_metadata {
    unsigned long int size;
    // TODO add more fields whenever necessary
};

static int snccloud_writeFileMeta(int fd, struct file_metadata &fm) {
    if (!(
        write(fd, &fm.size, sizeof(fm.size)) == sizeof(fm.size) 
        /* TODO write more fields whenever necessary */
    ))
        return errno;
    return 0;
}

static int snccloud_readFileMeta(const char *fpath, struct file_metadata &fm) {
    int fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return errno;
    int ret = 0;
    if (!(
        read(fd, &fm.size, sizeof(fm.size)) == sizeof(fm.size)
        /* TODO read more fields whenever necessary */
    ))
        ret = errno;
    close(fd);
    return ret;
}



static int snccloud_getFilePath(char *fpath, const char *path) {
    int total = strlen(path) + strlen(metadir) + strlen(parentdir) + 1 /* for / */;
    // check if the file name is too long
    if (total + 1 /* for \0 */ > PATH_MAX) {
        return -ENAMETOOLONG; 
    }
    memcpy(fpath, parentdir, strlen(parentdir));
    memcpy(fpath + strlen(parentdir), "/", 1);
    memcpy(fpath + strlen(parentdir) + 1, metadir, strlen(metadir));
    memcpy(fpath + strlen(parentdir) + 1 + strlen(metadir), path, strlen(path));
    fpath[total] = '\0';
    DLOG(INFO) << "File: " << fpath;
    return 0;
}

static void* snccloud_init(struct fuse_conn_info *conn) {
    cache = new Cache();
    mkdir(metadir, 0755);
    return 0;
}


static int snccloud_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    // create a local record file for the new file
    int fd = creat(fpath, mode);
    if (err)
        return err;

    // set up the record
    struct file_metadata fm;
    fm.size = 0;
    snccloud_writeFileMeta(fd, fm);

    close(fd);

    return 0;
}

static int snccloud_open(const char *path, struct fuse_file_info *fi) {
    bool fileExists = false;
    int fd;
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    // check local records to see if the file exists
    fd = open(fpath, fi->flags);
    fileExists = (fd >= 0) ? true : false;
    close(fd);

    if (!fileExists)
        return -ENOENT;
    // Recover file to .cache from .storage
    else
        cache->reconstructFile(path, strlen(path));

    return 0;
}

static int snccloud_write(const char *path, const char *data, size_t length, off_t offset, struct fuse_file_info *fi) {
    bool isAppend = false;

    DLOG(INFO) << "Writing file";
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    // read the local record
    struct file_metadata fm;
    err = snccloud_readFileMeta(fpath, fm);
    if (err != 0){
        DLOG(WARNING) << "Cannot read file metadata";
        return -ENOENT;
    }

    // TODO check if this is really an append operation, but not update or random write in a file
    if (fm.size == (unsigned long int) offset) {
        isAppend = true;
    }

    // refuse write if it is not an append
    DLOG(INFO) << "isAppend " << bool (isAppend);
    if (!isAppend) {
        return -1;
    }

    // append data to cache
    DLOG(INFO) << "Writing file with data length " << length;
    unsigned long int fileSize = cache->appendFile(path, strlen(path), data, length);
    DLOG(INFO) << "Written file with filesize " << fileSize;

    // update local record
    int fd = open(fpath, fi->flags | O_TRUNC);
    fm.size = fileSize;
    err = snccloud_writeFileMeta(fd, fm);
    close(fd);
    if (err != 0)
        return -ENOENT;

    return length;
} 

static int snccloud_unlink(const char *path) {
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    cache->deleteFile(path, strlen(path));
    return unlink(fpath);
}

static int snccloud_flush(const char *path, struct fuse_file_info *fi) {
    return 0;
}
 
static int snccloud_read(const char *path, char *data, size_t length, off_t offset, struct fuse_file_info *fi) {
    // size_t length is total number of blocks times block size
    // The smallest block size in linux filesystem will be 4096 and length will always be 4096 if file size is small
    // (Remarks by James, but works fine even if return value is smaller than 4K for Helen)
    // (FUSE file read function does not allow user to return readLength wihch is smaller than block size of system)
    // (Otherwise it will cause error when reading file)

    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    struct file_metadata fm;
    err = snccloud_readFileMeta(fpath, fm);
    if (err != 0){
        DLOG(WARNING) << "Cannot read file metadata";
        return -ENOENT;
    }

    DLOG(INFO) << "Reading file size " << fm.size;
    // It is not neccessary to change length to actual file size as fread() will stop when it meets EOF
    unsigned long int readLength = cache->readFile(path, strlen(path), offset, length, data);

    /*
    struct stat file_stat;
    stat("/", &file_stat);
    DLOG(INFO) << "Block size: " << file_stat.st_blksize;

    if (readLength < (unsigned long int) file_stat.st_blksize){
        readLength = (unsigned long int) file_stat.st_blksize;
    }
    */
    return readLength;
}

static int snccloud_release(const char *path, struct fuse_file_info *fi) {
    // Release is called when FUSE is completely done with a file
    // Exactly one release per open
    DLOG(INFO) << "FUSE: Release file";
    cache->commitFile(path, strlen(path));
    return 0;
}


static int snccloud_access(const char *path, int mask) {
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }
    return access(fpath, mask);
}

static int snccloud_getattr(const char *path, struct stat *stbuf) {
    // Root Directory
    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_uid = getuid();
        stbuf->st_gid = getgid();
        return 0;
    }

    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    // whether a local record exists
    int retstat = lstat(fpath, stbuf);
    if (retstat < 0)
        return -ENOENT;

    // do nothing on directories
    if (S_ISDIR(stbuf->st_mode))
        return 0;

    // read the local record
    struct file_metadata fm;
    err = snccloud_readFileMeta(fpath, fm);
    if (err != 0)
        return -ENOENT;

    // set the file size (and any other information) into stbuf, such that the command 'ls -lh' shows correct information
    stbuf->st_size = fm.size;

    return 0;
}

static int snccloud_utime(const char *path, struct utimbuf *ubuf) {
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    return utime(fpath, ubuf);
}


static int snccloud_opendir(const char *path, struct fuse_file_info *fi) {
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }
    DIR* dp = opendir(fpath);
    if(dp == NULL)
        return -errno;
    else fi->fh = (intptr_t)dp;
    return 0;
}

static int snccloud_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;

    dp = (DIR *) (uintptr_t) fi->fh;

    if ((de = readdir(dp)) == 0) {
        return -errno;
    }

    do {
        if (filler(buf, de->d_name, NULL, 0) != 0) {
            return -ENOMEM;
        }
    } while ((de = readdir(dp)) != NULL);

    return 0;
}

static int snccloud_mkdir(const char *path, mode_t mode){
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    int retstat = mkdir(fpath, mode);
    if(retstat < 0){
        return -errno;
    }
    return retstat;
}

static int snccloud_rmdir(const char *path){
    char fpath[PATH_MAX];
    int err = snccloud_getFilePath(fpath, path);
    if (err != 0){
        DLOG(ERROR) << "Get File Path error " << err;
        return err;
    }

    int retstat = rmdir(fpath);
    if(retstat < 0){
        return -errno;
    }
    return retstat;
}


struct snccloud_fuse_operations: fuse_operations {
    snccloud_fuse_operations() {
        init = snccloud_init;

        create = snccloud_create;
        open = snccloud_open;
        write = snccloud_write;
        unlink = snccloud_unlink;
        flush = snccloud_flush;
        read = snccloud_read;
        release = snccloud_release;

        access = snccloud_access;
        getattr = snccloud_getattr;
        utime = snccloud_utime;

        opendir = snccloud_opendir;
        readdir = snccloud_readdir;
        mkdir = snccloud_mkdir;
        rmdir = snccloud_rmdir;
    }
};

static struct snccloud_fuse_operations snccloud_oper;

void snccloud_fuse_usage(char* p) {
    DLOG(ERROR) << p << " [FUSE options] mount_point";
    exit(1);
}

int main(int argc, char **argv) {
    mkdir("./logfile", 0755);
    FLAGS_log_dir = "./logfile/";
    google::InitGoogleLogging(argv[0]);

    if (getcwd(parentdir, PATH_MAX) == NULL)
        return errno;

    if (argc < 2)
        snccloud_fuse_usage(argv[0]);

    return fuse_main(argc, argv, &snccloud_oper, 0);
}
