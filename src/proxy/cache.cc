#include <dirent.h> // DIR
#include <stdio.h>
#include <string.h> // strlen()
#include <unistd.h> // lstat(), unlink()
#include <sys/types.h> // lstat()
#include <sys/stat.h> // mkdir(), lstat()
#include <iostream>

#include <glog/logging.h> // logging library

#include "cache.hh"
#include "../aont/encode.hh"
#include "../aont/decode.hh"
#include "../aont/aont.hh"
#include "../aont/rscode.hh"

#define CACHE_DIR ".cache"
#define STORAGE_DIR ".storage"

using namespace std;

Cache::Cache() {
    //_proxy = new Proxy();
    mkdir(CACHE_DIR, 0755);
    DIR *d = opendir(CACHE_DIR);
    if (d == NULL) {
        if (mkdir(CACHE_DIR, 0755) != 0) {
            LOG(ERROR) << "Failed to create cache directory" << CACHE_DIR;
            exit(1);
        }
    } else {
        closedir(d);
    }

    mkdir(STORAGE_DIR, 0755);
    d = opendir(STORAGE_DIR);
    if (d == NULL) {
        if (mkdir(STORAGE_DIR, 0755) != 0) {
            LOG(ERROR) << "Failed to create storage directory" << STORAGE_DIR;
            exit(1);
        }
    } else {
        closedir(d);
    }
}

Cache::~Cache() {
}

void Cache::getFileCachePath(char *cacheFilename, const char *filename){
    strcpy(cacheFilename, filename);

    for (unsigned int i = 1; i < strlen(filename); i++){
        if (cacheFilename[i] == '/')
            cacheFilename[i] = '\n';
    }
    return;
}

unsigned long int Cache::appendFile(const char *filename, unsigned short filenameLength, const char *data, unsigned long int length) {
    char cacheFilename[filenameLength];
    getFileCachePath(cacheFilename, filename);

    int pathLen = strlen(CACHE_DIR) + filenameLength + 1;
    char path[pathLen];
    sprintf(path, "%s%s", CACHE_DIR, cacheFilename);

    FILE *f = fopen(path, "a");
    if (f == NULL)
        return 0;
    
    unsigned long int written = 0;
    size_t ret = fwrite(data + written, 1, length - written, f);
    DLOG(INFO) << "Writing " << ret << " bytes";
    written += ret;
    fclose(f);
    
    // return current file size
    struct stat sbuf;
    lstat(path, &sbuf);
    DLOG(INFO) << "Written " << sbuf.st_size << " bytes";
    return sbuf.st_size;
}

unsigned long int Cache::commitFile(const char *filename, unsigned short filenameLength) {
    char cacheFilename[filenameLength];
    getFileCachePath(cacheFilename, filename);

    int pathLen = strlen(CACHE_DIR) + filenameLength + 1;
    char path[pathLen];
    sprintf(path, "%s%s", CACHE_DIR, cacheFilename);

    pathLen = strlen(STORAGE_DIR) + filenameLength + 1;
    char storage_path[pathLen];
    sprintf(storage_path, "%s%s", STORAGE_DIR, cacheFilename);

    // TODO read file and call proxy
    CAONT_Encode *encoder = new CAONT_Encode(32);
    bool readfile_ret = encoder->readFromFile(path);
    if (readfile_ret == false) {
        cout << "Read file unsuccessfully" << endl;
        exit(1);
    }

    bool encoder_ret = encoder->encodeFile(1);
    if (encoder_ret == false) {
        cout << "Encoded unsuccessfully" << endl;
        exit(1);
    }

    encoder->writetofile(storage_path);
    delete encoder;

    struct stat sbuf;
    lstat(path, &sbuf);

    // remove the cache
    unlink(path);

    return sbuf.st_size;
}

unsigned long int Cache::readFile(const char *filename, unsigned short filenameLength, unsigned long int offset, unsigned long int length, char *data) {
    char cacheFilename[filenameLength];
    getFileCachePath(cacheFilename, filename);

    // TODO read directly from storage and decode
    int pathLen = strlen(CACHE_DIR) + filenameLength + 1;
    char path[pathLen];
    sprintf(path, "%s%s", CACHE_DIR, cacheFilename);

    DLOG(INFO) << "Read from path: " << path;
    DLOG(INFO) << "Read with length " << length << " and offset " << offset;

    FILE *fp = fopen(path, "r");
    if (fp == NULL)
        return 0;

    fseek(fp, offset, SEEK_SET);

    size_t ret = 0;
    DLOG(INFO) << "Reading from file";
    ret = fread(data, sizeof(char), length, fp);
    DLOG(INFO) << "read data size: " << ret;

    if(ret > length){
        DLOG(WARNING) << "Size and return value mismatch";
    }
    fclose(fp);

    return ret;
}

bool Cache::deleteFile(const char *filename, unsigned short filenameLength) {
    char cacheFilename[filenameLength];
    getFileCachePath(cacheFilename, filename);

    // remove file cache, and then data (chunks) and file metadata
    int pathLen = strlen(CACHE_DIR) + filenameLength + 1;
    char path[pathLen];
    sprintf(path, "%s%s", CACHE_DIR, cacheFilename);

    pathLen = strlen(STORAGE_DIR) + filenameLength + 1;
    char storage_path[pathLen];
    sprintf(storage_path, "%s%s", STORAGE_DIR, cacheFilename);

    unlink(path);
    unlink(storage_path);
    return 0;
}

bool Cache::reconstructFile(const char *filename, unsigned short filenameLength) {
    char cacheFilename[filenameLength];
    getFileCachePath(cacheFilename, filename);

    int pathLen = strlen(CACHE_DIR) + filenameLength + 1;
    char path[pathLen];
    sprintf(path, "%s%s", CACHE_DIR, cacheFilename);

    pathLen = strlen(STORAGE_DIR) + filenameLength + 1;
    char storage_path[pathLen];
    sprintf(storage_path, "%s%s", STORAGE_DIR, cacheFilename);

    CAONT_Decode *decoder = new CAONT_Decode(32);
    bool decoder_ret = decoder->decodeFile(storage_path);
    if (decoder_ret == false) {
        cout << "Decoded unsuccessfully" << endl;
        exit(1);
    }

    decoder->writetofile(path);

    delete decoder;
    return decoder_ret;
}
