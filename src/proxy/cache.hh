#ifndef __CACHE_HH__
#define __CACHE_HH__

#include "proxy.hh"

class Cache {
public:
    Cache();
    ~Cache();

    // path rename
    virtual void getFileCachePath(char *fpath, const char *path);

    // write
    virtual unsigned long int appendFile(const char *filename, unsigned short filenameLength, const char *data, unsigned long int length);
    virtual unsigned long int commitFile(const char *filename, unsigned short filenameLength);
    // read
    virtual unsigned long int readFile(const char *filename, unsigned short filenameLength, unsigned long int offset, unsigned long int length, char *data);
    // delete
    virtual bool deleteFile(const char *filename, unsigned short filenameLength);
    // reconstruct
    virtual bool reconstructFile(const char *filename, unsigned short filenameLength);

protected:
    Proxy *_proxy;

};
#endif // define __CACHE_HH__
