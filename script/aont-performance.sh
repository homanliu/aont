#!/bin/bash
# Copy into build directory first
# Then run this script to test with below file sizes

md5sum_check(){
    # Get md5sum of original file and decoded file
    generateFile=$1
    outputFile=$2
    originalFileMD5=`md5sum ${generateFile} | awk '{ print $1 }'`
    decodedFileMD5=`md5sum ${outputFile} | awk '{ print $1 }'`

    if [ "$originalFileMD5" != "$decodedFileMD5" ]; then
        printf "${RED}\t\tFAIL\n${NC}"
        echo -e "${RED}Original file md5sum: "$originalFileMD5"${NC}"
        echo -e "${RED}Decoded file md5sum: "$decodedFileMD5"${NC}"
        exit 1
    fi
}

RED=$'\e[1;31m'
GREEN=$'\e[1;32m'
NC=$'\e[0m'

readonly fileSize=("1" "10" "20" "50" "100" "200" "500")

for (( i=0; i < ${#fileSize[@]}; ++i )); do
    printf "Input Size: %s\n" ${fileSize[$i]}"MB"

    inputFile="testcase"${fileSize[$i]}".txt"
    encodedFile="encoded"${fileSize[$i]}".txt"
    decodedFile="decoded"${fileSize[$i]}".txt"


    # Generate file with size
    ( dd if=/dev/urandom of=$inputFile bs=1M count=${fileSize[i]} ) > /dev/null 2>&1

    ./aont --encode $inputFile $encodedFile $1 $2
    ./aont --decode $encodedFile $decodedFile $1 $2

    # echo "Program returned"
    md5sum_check $inputFile $decodedFile

    rm $inputFile $encodedFile $decodedFile

    printf "${GREEN}PASS\n${NC}"
done
