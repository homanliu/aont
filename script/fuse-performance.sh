#!/bin/bash
# Copy into build directory first and mount FUSE file system in another terminal
# Then run this script to test with below file sizes
# Cannot mount FUSE file system in single bash script file

md5sum_check(){
    # Get md5sum of original file and file in FUSE
    generateFile=$1
    uploadedFile=$2
    testNumber=$3
    originalFile=`md5sum ${generateFile} | awk '{ print $1 }'`
    mountdirFile=`md5sum ${uploadedFile} | awk '{ print $1 }'`

    if [ "$originalFile" != "$mountdirFile" ]; then
        printf "${RED}%s     FAIL\n${NC}" "${testNumber}"
        echo -e "${RED}Original file md5sum: "$originalFile"${NC}"
        echo -e "${RED}Mountdir file md5sum: "$mountdirFile"${NC}"
        exit 1
    else
        printf "%s     " "${GREEN}${testNumber}${NC}"
    fi
}

filesize_check(){
    generateFile=$1
    uploadedFile=$2
    testNumber=$3
    # fileSize_Original=`du -k ${generateFile} | cut -f1`
    # fileSize_Metadata=`du -k ${uploadedFile} | cut -f1`
    fileSize_Original=`ls -la ${generateFile} | awk '{print $5}'`
    fileSize_Metadata=`ls -la ${uploadedFile} | awk '{print $5}'`
    if [ "$fileSize_Original" != "$fileSize_Metadata" ]; then
        printf "${RED}%s     FAIL\n${NC}" "${testNumber}"
        echo -e "${RED}Original file size: "$fileSize_Original"${NC}"
        echo -e "${RED}Metadata file size: "$fileSize_Metadata"${NC}"
        exit 1
    else
        printf "%s     " "${GREEN}${testNumber}${NC}"
    fi
}

delete_check(){
    FILE=$1
    if [ -f $FILE ]; then
        printf "${RED}5     FAIL\n${NC}"
        echo -e "${RED}${FILE} exists${NC}"
        exit 1
    fi
}

RED=$'\e[1;31m'
GREEN=$'\e[1;32m'
NC=$'\e[0m'

echo "FUSE file system tests on different file sizes"
echo "Operations:"
echo "1. Upload file"
echo "2. File size check"
echo "3. Text append to existing file"
echo "4. File size check for modified file"
echo "5. Delete file"
echo

readonly fileSize=("1" "10" "20" "50" "100" "200" "500")

mkdir -p mountdir/testdir/

for (( i=0; i < ${#fileSize[@]}; ++i )); do
    printf "%s\n" ${fileSize[$i]}"MB"

    generateFile="testcase"${fileSize[$i]}".txt"

    # Generate file with size
    ( dd if=/dev/urandom of=$generateFile bs=1M count=${fileSize[i]} ) > /dev/null 2>&1

    START_TIME=$(date +%s%N)
    cp $generateFile mountdir/
    ELAPSED_TIME=$((($(date +%s%N) - $START_TIME)/1000000))

    md5sum_check $generateFile mountdir/$generateFile "1"
    filesize_check $generateFile mountdir/$generateFile "2"
    
    rm mountdir/$generateFile
    delete_check mountdir/$generateFile
    delete_check metadata/$generateFile
    delete_check .cache/$generateFile
    printf "${GREEN}5     PASS\n${NC}"

    echo "Runtime: "$ELAPSED_TIME" ms"
    SPEED=$((${fileSize[$i]}*1000/$ELAPSED_TIME))
    echo "Write Speed: "$SPEED" MB/s"

    rm $generateFile

done

rm -r mountdir/testdir/