#!/bin/bash
# Copy into build directory first and mount FUSE file system in another terminal
# Then run this script to test with below file sizes
# Cannot mount FUSE file system in single bash script file

md5sum_check(){
    # Get md5sum of original file and file in FUSE
    generateFile=$1
    uploadedFile=$2
    testNumber=$3
    originalFile=`md5sum ${generateFile} | awk '{ print $1 }'`
    mountdirFile=`md5sum ${uploadedFile} | awk '{ print $1 }'`

    if [ "$originalFile" != "$mountdirFile" ]; then
        printf "${RED}%s     FAIL\n${NC}" "${testNumber}"
        echo -e "${RED}Original file md5sum: "$originalFile"${NC}"
        echo -e "${RED}Mountdir file md5sum: "$mountdirFile"${NC}"
        exit 1
    else
        printf "%s     " "${GREEN}${testNumber}${NC}"
    fi
}

filesize_check(){
    generateFile=$1
    uploadedFile=$2
    testNumber=$3
    # fileSize_Original=`du -k ${generateFile} | cut -f1`
    # fileSize_Metadata=`du -k ${uploadedFile} | cut -f1`
    fileSize_Original=`ls -la ${generateFile} | awk '{print $5}'`
    fileSize_Metadata=`ls -la ${uploadedFile} | awk '{print $5}'`
    if [ "$fileSize_Original" != "$fileSize_Metadata" ]; then
        printf "${RED}%s     FAIL\n${NC}" "${testNumber}"
        echo -e "${RED}Original file size: "$fileSize_Original"${NC}"
        echo -e "${RED}Metadata file size: "$fileSize_Metadata"${NC}"
        exit 1
    else
        printf "%s     " "${GREEN}${testNumber}${NC}"
    fi
}

delete_check(){
    FILE=$1
    if [ -f $FILE ]; then
        printf "${RED}5     FAIL\n${NC}"
        echo -e "${RED}${FILE} exists${NC}"
        exit 1
    fi
}

RED=$'\e[1;31m'
GREEN=$'\e[1;32m'
NC=$'\e[0m'

echo "FUSE file system tests on different file sizes"
echo "Operations:"
echo "1. Upload file"
echo "2. File size check"
echo "3. Text append to existing file"
echo "4. File size check for modified file"
echo "5. Delete file"
echo

readonly fileSize=("100" "500" "1K" "5K" "8K" "10K" "50K" "80K" "100K" "200K" "500K" "1M")

printf "Create directory on FUSE filesystem"
mkdir -p mountdir/testdir/
if [[ ( -d "./mountdir/testdir/" ) && ( -d "./metadata/testdir/" ) ]]; then
    printf "${GREEN}     PASS\n${NC}"
else
    printf "${RED}     FAIL\n${NC}"
    exit 1
fi

printf "Remove directory on FUSE filesystem"
rm -r mountdir/testdir/
if [[ ( -d "./mountdir/testdir/" ) || ( -d "./metadata/testdir/" ) ]]; then
    printf "${RED}     FAIL\n${NC}"
    exit 1
else
    printf "${GREEN}     PASS\n${NC}"
fi

echo
mkdir -p mountdir/testdir/

for (( i=0; i < ${#fileSize[@]}; ++i )); do
    printf "%s\n" ${fileSize[$i]}"B"

    generateFile="testcase"${fileSize[$i]}".txt"

    # Generate file with size
    ( dd if=/dev/urandom of=$generateFile bs=${fileSize[i]} count=1 ) > /dev/null 2>&1

    cp $generateFile mountdir/
    md5sum_check $generateFile mountdir/$generateFile "1"
    filesize_check $generateFile mountdir/$generateFile "2"

    randomString=`head -3 /dev/urandom | tr -cd '[:alnum:]' | cut -c -$RANDOM`
    echo $randomString >> ${generateFile}
    echo $randomString >> mountdir/${generateFile}
    md5sum_check $generateFile mountdir/$generateFile "3"
    filesize_check $generateFile mountdir/$generateFile "4"
    
    rm mountdir/$generateFile
    delete_check mountdir/$generateFile
    delete_check metadata/$generateFile
    delete_check .cache/$generateFile
    printf "${GREEN}5     PASS\n${NC}"


    printf "%s\n" ${fileSize[$i]}"B (Directory)"
    cp $generateFile mountdir/testdir/
    md5sum_check $generateFile mountdir/testdir/$generateFile "1"
    filesize_check $generateFile mountdir/testdir/$generateFile "2"

    randomString=`head -3 /dev/urandom | tr -cd '[:alnum:]' | cut -c -$RANDOM`
    echo $randomString >> ${generateFile}
    echo $randomString >> mountdir/testdir/${generateFile}
    md5sum_check $generateFile mountdir/testdir/$generateFile "3"
    filesize_check $generateFile mountdir/testdir/$generateFile "4"
    
    rm mountdir/testdir/$generateFile
    delete_check mountdir/testdir/$generateFile
    delete_check metadata/testdir/$generateFile
    delete_check .cache/testdir?$generateFile
    printf "${GREEN}5     PASS\n${NC}"

    rm $generateFile

done

rm -r mountdir/testdir/