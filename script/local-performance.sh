#!/bin/bash

RED=$'\e[1;31m'
GREEN=$'\e[1;32m'
NC=$'\e[0m'

readonly fileSize=("1" "10" "20" "50" "100" "200" "500")

mkdir -p testdir/

for (( i=0; i < ${#fileSize[@]}; ++i )); do
    printf "%s\n" ${fileSize[$i]}"MB"

    generateFile="testcase"${fileSize[$i]}".txt"

    # Generate file with size
    ( dd if=/dev/urandom of=$generateFile bs=1M count=${fileSize[i]} ) > /dev/null 2>&1

    START_TIME=$(date +%s%N)
    cp $generateFile testdir/
    ELAPSED_TIME=$((($(date +%s%N) - $START_TIME)/1000000))
    
    rm testdir/$generateFile

    echo "Runtime: "$ELAPSED_TIME" ms"
    SPEED=$((${fileSize[$i]}*1000/$ELAPSED_TIME))
    echo "Write Speed: "$SPEED" MB/s"

    rm $generateFile

done

rm -r testdir/