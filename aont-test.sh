#!/bin/bash
# Copy into build directory first
# Then run this script to test with below file sizes

md5sum_check(){
    # Get md5sum of original file and decoded file
    generateFile=$1
    outputFile=$2
    originalFileMD5=`md5sum ${generateFile} | awk '{ print $1 }'`
    decodedFileMD5=`md5sum ${outputFile} | awk '{ print $1 }'`

    if [ "$originalFileMD5" != "$decodedFileMD5" ]; then
        printf "${RED}\t\tFAIL\n${NC}"
        echo -e "${RED}Original file md5sum: "$originalFileMD5"${NC}"
        echo -e "${RED}Decoded file md5sum: "$decodedFileMD5"${NC}"
        exit 1
    fi
}

RED=$'\e[1;31m'
GREEN=$'\e[1;32m'
NC=$'\e[0m'

readonly fileSize=("100" "500" "1K" "5K" "8K" "10K" "50K" "80K" "100K" "200K" "500K" "1M" "10M")

for (( i=0; i < ${#fileSize[@]}; ++i )); do
    printf "%s " ${fileSize[$i]}"B"

    inputFile="testcase"${fileSize[$i]}".txt"
    encodedFile="encoded"${fileSize[$i]}".txt"
    decodedFile="decoded"${fileSize[$i]}".txt"


    # Generate file with size
    ( dd if=/dev/urandom of=$inputFile bs=${fileSize[i]} count=1 ) > /dev/null 2>&1

    START_ENCODE_TIME=$(date +%s%N)
    ./aont --encode $inputFile $encodedFile 2
    ELAPSED_ENCODE_TIME=$((($(date +%s%N) - $START_ENCODE_TIME)/1000000))
    START_DECODE_TIME=$(date +%s%N)
    ./aont --decode $encodedFile $decodedFile 2
    ELAPSED_DECODE_TIME=$((($(date +%s%N) - $START_DECODE_TIME)/1000000))
    

    # echo "Program returned"
    md5sum_check $inputFile $decodedFile

    randomString=`head -3 /dev/urandom | tr -cd '[:alnum:]' | cut -c -$RANDOM`
    printf "(+ %s) " ${#randomString}"B"
    echo $randomString >> ${inputFile}

    ./aont --encode $inputFile $encodedFile 2
    ./aont --decode $encodedFile $decodedFile 2
    # echo "Program returned"
    md5sum_check $inputFile $decodedFile

    rm $inputFile $encodedFile $decodedFile

    printf "${GREEN}\t\tPASS\n${NC}"
    echo "Encoder Runtime: "$ELAPSED_ENCODE_TIME"ms"
    echo "Decoder Runtime: "$ELAPSED_DECODE_TIME"ms"
done
